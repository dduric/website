<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supercoolfirma | Kontakt</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/moj.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
    <div class="about">
        <?php
            include 'header.html';
        ?>
        <div class="row">
            <div class="col-sm-6">
                <h2><strong>Kontaktirajte nas:</strong></h2>
                <strong>Telefon:</strong>
                <p>+387 31 712 359</p>
                <strong>Email</strong>
                <p>info@supercoolfirma.hr</p>
            </div>
            <div class="col-sm-6">
                <h3><strong>Adresa</strong></h3>
                <h4>Supercoolfirma d.o.o</h4>
                <p>Donjomahalska 420</p>
                <p>31000 Osijek</p>
                <p>Hrvatska</p>
            </div>
        </div>
    </div>
</body>
<footer>
    <div class="footer">
        <p>Potražite nas i na društvenim mrežama</p>
        <a href="https://www.facebook.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124010.svg" height="30" width="30"></a>
        <a href="https://www.instagram.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124032.svg" height="30" width="30"></a>
        <a href="https://www.linkedin.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124011.svg" height="30" width="30"></a>
    </div>
</footer>