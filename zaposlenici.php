<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supercoolfirma | Zaposlenici</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/moj.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="zaposlenici">
        <?php
            include 'header.html';
        ?>

    <h2><strong>Supercool tim</strong></h2>

    <table>
    <tr>
        <th>Ime</th>
        <th>Prezime</th>
        <th>Email</th>
    </tr>
    <?php
    require_once("connection.php");
    session_start();
    $error = "";

    $query = "SELECT * FROM zaposlenici";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
        	echo "<tr><td>".$row["ime"]."</td><td>".$row["prezime"]."</td><td>".$row["email"]."</td><td>";
        }   
    }
    ?>
    </table>
</div>
</body>
<footer>
    <div class="footer">
        <p>Potražite nas i na društvenim mrežama</p>
        <a href="https://www.facebook.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124010.svg" height="30" width="30"></a>
        <a href="https://www.instagram.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124032.svg" height="30" width="30"></a>
        <a href="https://www.linkedin.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124011.svg" height="30" width="30"></a>

    </div>
</footer>