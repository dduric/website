<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Supercoolfirma | Login</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/login.css">
<link rel="stylesheet" href="css/moj.css">
</head>
<body>
    <div class="portfolio">
        <?php
            include 'header.html';
        ?>

    <div class="containter">
        <h2><strong>Login Form</strong></h2>
        <form action="" method ="post">
            <label>Username</label>
            <br>
            <input type="text" placeholder="Username" name="username" required/>
            <br>
            <label>Password</label>
            <br>
            <input type="password" placeholder="Password" name="password" required/>
            <br>
            <br>
            <input type="submit" value="Login" name="submit" id="login"/>
            <br>
            
        </form>
<?php
require_once("connection.php");
session_start();
$error = "";

if(isset($_POST['submit'])){
    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = "SELECT * FROM korisnici WHERE username='$username' AND password = '$password'";

    $result = mysqli_query($conn, $query);

    if(mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);
        $_SESSION['username'] = $username;
        header("location: admin.php");
    } else {
        $error = "Pogrešan username ili password";
    }
}
mysqli_close($conn);
?>
<h2><?php echo $error; ?></h2> 
<br>
</div>
</div>
</body>
</html>
<footer>
    <div class="footer">
        <p>Potražite nas i na društvenim mrežama</p>
        <a href="https://www.facebook.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124010.svg" height="30" width="30"></a>
        <a href="https://www.instagram.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124032.svg" height="30" width="30"></a>
        <a href="https://www.linkedin.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124011.svg" height="30" width="30"></a>
    </div>
</footer>