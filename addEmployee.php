<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Employee</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/moj.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
    <div class="about">
        <?php
            include 'header.html';
            require_once("connection.php");
            session_start();
            $error = "";

            if(isset($_SESSION['username'])){
                } else{
                    header("location: login.php");
                }
?>

<html>
<head></head>
<body>
    <h3>Dodaj zaposlenika</h3>
    <form action="addEmployeePOST.php" method="post">
        <label>Ime zaposlenika: </label><input type="text" name="ime" /> <br>
        <label>Prezime zaposlenika: </label><input type="text" name="prezime" /> <br>
        <label>Email: </label><input type="email" name="email" /> <br>

        <input type="submit" value="Unesi" name="submit" />
    </form>

    <h2><?php if ($error != ""){
        echo "<p>$error</p>";
    }
    ?></h2>

</body>
</html>

<?php mysqli_close($conn);
?>

<footer>
    <div class="footer">
        <p>Potražite nas i na društvenim mrežama</p>
        <a href="https://www.facebook.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124010.svg" height="30" width="30"></a>
        <a href="https://www.instagram.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124032.svg" height="30" width="30"></a>
        <a href="https://www.linkedin.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124011.svg" height="30" width="30"></a>
    </div>
</footer>