<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supercoolfirma</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/moj.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>
            function getTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
                t = setTimeout('getTime()', 500);
            }
            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            }
        </script>
</head>
<body>
    <div class="body">
        <?php
            include 'header.html';
        ?>

        <div class="row" id="main">
            <div class="col-sm-12">
                <div class="intro">
                    <h1><strong>Supercoolfirma</strong></h1>
                    <h3><strong>Backend & frontend development</strong></h3>
                    <h5><a href="about.html">Saznajte više</a></h5>    
                </div>            
            </div>
        </div>

        <div class="fixed" id="hclogo">
            <img src="https://i.ibb.co/Qm50XZL/logo.png" onmouseover="getTime()" alt="logo" class height="90" width="85">
            <label id="time"></label>
        </div>

    </div>
</body>
<footer>
    <div class="footer">
        <p>Potražite nas i na društvenim mrežama</p>
        <a href="https://www.facebook.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124010.svg" height="30" width="30"></a>
        <a href="https://www.instagram.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124032.svg" height="30" width="30"></a>
        <a href="https://www.linkedin.com/supercoolfirma"><img src="https://image.flaticon.com/icons/svg/124/124011.svg" height="30" width="30"></a>
    </div>
</footer>

</html>